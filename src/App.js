import React, { Component } from 'react';
import styled from 'styled-components';
import './App.css';
import Person from './Person/Person';

const StyledButton = styled.button`
      background-color: ${props => props.alter ? 'red' : 'green'};
      color: white;
      font: inherit;
      border: 1px solid purple;
      padding: 8px;
      cursor: pointer;

      &:hover {
        background-color: lightgreen;
        color: black;
      }

`;

class App extends Component {
  state = {
    persons: [
      { id: 'ldajslfjd', name: 'person-1', age: 11 },
      { id: 'jklajf', name: 'person-2', age: 22 },
      { id: 'kjljsls', name: 'person-3', age: 33 }
    ],
    showPersonsInd: false
  }

  toggleNamesHandler = () => {
    const doesShow = this.state.showPersonsInd;
    this.setState({ showPersonsInd: !doesShow })
  }

  deleteNamesHandler = personsIndex => {
    const persons = [...this.state.persons];
    persons.splice(personsIndex, 1)
    this.setState({ persons: persons })
  }

  nameChangedHandler = (event, id) => {
    const personsIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    })

    const person = { ...this.state.persons[personsIndex] }
    person.name = event.target.value;

    const persons = [...this.state.persons]
    persons[personsIndex] = person;

    this.setState({ persons: persons })

  }


  render() {
    let persons = null;

    if (this.state.showPersonsInd) {
      persons = (
        <div>
          {this.state.persons.map((person, index) => {
            return <Person
              click={() => this.deleteNamesHandler(person.id)}
              name={person.name}
              age={person.age}
              key={person.id}
              changed={(event) => this.nameChangedHandler(event, person.id)}
            />
          })}
        </div>
      )
    }
    return (
      <div className="App">
        <StyledButton alter={this.state.showPersonsInd} onClick={this.toggleNamesHandler}>Switch Names</StyledButton>
        {persons}
      </div>
    )
  }
}

export default App;
