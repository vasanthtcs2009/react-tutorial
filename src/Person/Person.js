import React from 'react';
import './Person.css'
import styled from 'styled-components'

const StyledDiv = styled.div`
        background-color: bisque;
        align-content: center;
        border: 5px solid purple;
        width: 20%;
        margin: 12px auto;
        box-shadow: 0 2px 3px greenyellow;
        padding: 5px;
        text-align: center;
        @media (min-width:400px){
            width:400px
    }`;

const Person = props => {
    return (
        <StyledDiv>
            <p onClick={props.click} > I'm {props.name} and I am {props.age} years old!</p>
            <input type="text" onChange={props.changed} value={props.name} />
        </StyledDiv>
    )
}

export default Person;